import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildParentViewchildModule } from './child-parent-viewchild/child-parent-viewchild.module';
import { ChildParentModule } from './child-parent/child-parent.module';
import { ParentChildModule } from './parent-child/parent-child.module';
import { ServiceModule } from './service/service.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ParentChildModule,
    ChildParentModule,
    ChildParentViewchildModule,
    ServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
