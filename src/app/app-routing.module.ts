import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EcparentComponent } from './child-parent-viewchild/ecparent/ecparent.component';
import { HeaderComponent } from './child-parent/header/header.component';
import { TeacherComponent } from './parent-child/teacher/teacher.component';
import { BasiccheckComponent } from './service/basiccheck/basiccheck.component';

const routes: Routes = [
  { path: 'parent/child', component: TeacherComponent },
  { path: 'child/parent', component: HeaderComponent },
  { path: 'child-parent/viewchild', component: EcparentComponent },
  { path: 'service', component: BasiccheckComponent },
  { path: '', redirectTo: 'parent/child', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
