import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasiccheckComponent } from '../service/basiccheck/basiccheck.component';
import { AdvancecheckComponent } from '../service/advancecheck/advancecheck.component';
import { FinalcheckComponent } from '../service/finalcheck/finalcheck.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    BasiccheckComponent,
    AdvancecheckComponent,
    FinalcheckComponent,
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class ServiceModule { }
