import { Component, Input, OnInit } from '@angular/core';
import { Teacher } from '../teacher';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  @Input() teacher: Teacher;
  @Input('priciple') pricipleName: string;
   
  constructor() { }

  ngOnInit(): void {
    //this.pricipleName = "jay"
  }

}
