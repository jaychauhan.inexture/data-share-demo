import { Component, OnInit } from '@angular/core';
import { StudentComponent } from '../student/student.component';
import { Teachers } from '../teacher';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css'],
})
export class TeacherComponent implements OnInit {
  teachers = Teachers;
  principle = 'Principle';

  constructor() { }

  ngOnInit(): void {
  }

}
