import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherComponent } from './teacher/teacher.component';
import { StudentComponent } from './student/student.component';



@NgModule({
  declarations: [
    TeacherComponent,
    StudentComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ParentChildModule { }
