import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName = "";

  constructor() { }

  ngOnInit(): void {
  }

  onLogin(user: string) {
    this.userName = user;
  }

}
