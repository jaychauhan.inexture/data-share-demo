import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ecchild',
  templateUrl: './ecchild.component.html',
  styleUrls: ['./ecchild.component.css']
})
export class EcchildComponent implements OnInit {
  intervalId = 0;
  message = '';
  seconds = 0;

  constructor() { }

  ngOnInit(): void {
    this.start();
  }

  ngOnDestroy() { this.clearTimer(); }

  clearTimer() { clearInterval(this.intervalId); }

  start() { this.countDown(); }

  stop() {
    this.clearTimer();
    this.message = `Holding at T-${this.seconds} seconds`;
  }

  private countDown() {
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds += 1;
      if (this.seconds === 0) {
        this.message = 'Completed counting!';
      } else {
        if (this.seconds < 0) { this.seconds = 50; } // reset
        this.message = `Vote-${this.seconds} and counting going on`;
      }
    }, 1000);
  }
}
