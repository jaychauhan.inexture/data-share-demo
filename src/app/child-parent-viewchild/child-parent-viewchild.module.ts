import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EcparentComponent } from './ecparent/ecparent.component';
import { EcchildComponent } from './ecchild/ecchild.component';



@NgModule({
  declarations: [
    EcparentComponent,
    EcchildComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ChildParentViewchildModule { }
